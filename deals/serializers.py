import csv
import os
from rest_framework import serializers, status
from .models import File, Client, Gem, Deal
from .utils import save_sum_gems, sum_gems, upload_from_csv_to_db


class GemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gem
        fields = ['name']


class ClientSerializer(serializers.ModelSerializer):
    gems = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name'
    )

    class Meta:
        model = Client
        fields = ['username', 'spent_money', 'gems']


class FileSerializer(serializers.ModelSerializer):
    deals = serializers.FileField(allow_empty_file=False, use_url=False)

    class Meta:
        model = File
        fields = ["deals", ]

    def create(self, validated_data):
        [model.objects.all().delete() for model in [File, Deal, Gem, Client]]

        deals = self.Meta.model(**validated_data)
        deals.save()

        WORKPATH = 'deals/media'

        try:
            with open(os.path.join(WORKPATH, "{}".format(str(validated_data['deals']))), encoding='utf-8') as f:
                upload_from_csv_to_db(f)
        except (FileNotFoundError, csv.Error) as e:
            error = {
                'Status': 'Error',
                'Desc': 'В процессе обработки файла произошла ошибка.'
            }
            raise serializers.ValidationError(error)

        save_sum_gems()

        sum_gems()

        return deals
