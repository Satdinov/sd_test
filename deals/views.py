from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from .models import File, Client
from rest_framework import viewsets
from .serializers import ClientSerializer, FileSerializer


class ClientViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Client.objects.all().order_by('-spent_money')[:5]
    serializer_class = ClientSerializer


class FileViewSet(mixins.CreateModelMixin, GenericViewSet):
    queryset = File.objects.all()
    serializer_class = FileSerializer
