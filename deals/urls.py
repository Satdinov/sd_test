from django.urls import path, include

from rest_framework import routers

from deals.views import ClientViewSet, FileViewSet

# from deals.views import ClientViewSet, FileViewSet

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'file', FileViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]