import csv

from rest_framework.exceptions import ValidationError

from .models import Deal, Client, Gem
from django.core.exceptions import ValidationError


def upload_from_csv_to_db(f):
    reader = csv.reader(f)
    next(reader)
    try:
        for row in reader:  # Считываем данные в таблицу
            Deal(
                customer=row[0],
                item=row[1],
                total=row[2],
                quantity=row[3],
                date=row[4]
            ).save()
    except Exception as e:
        error = {'Status': "Error",
                 "Desc": "<{}>- в процессе обработки файла произошла ошибка.".format(e.args)
                 if len(e.args) > 0 else 'Unknown Error'}
        raise ValidationError(error)

    f.close()


def save_sum_gems():
    customers = Deal.objects.all().values("customer")
    clients = []
    for customer in customers:
        clients.append(customer["customer"])

    for client in set(clients):  # Считаем сумму сделок для каждого клиента
        gems = []
        total_sum = 0
        client_query = list(Deal.objects.filter(customer=client))
        for query in client_query:
            total_sum += query.total
            gems.append(query.item)  # Сохраняем список товаров купленных клиентом

        client_model = Client(username=client,
                              spent_money=total_sum)
        client_model.save()
        for gem in set(gems):
            gem_model = Gem(name=gem)
            gem_model.save()
            client_model.gems.add(gem_model)


def sum_gems():
    gems_correct = list(Client.objects.all().order_by('-spent_money')[:5])
    gems_list = []

    for line in gems_correct:
        for gems in list(line.gems.all().values("name")):
            gems_list.append(gems['name'])

    gems_list = list(set([x for x in gems_list if gems_list.count(x) == 1]))

    for item in gems_list:
        Gem.objects.filter(name=item).delete()
