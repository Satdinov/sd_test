from django.conf.global_settings import MEDIA_URL
from django.db import models
import csv
from django.core.exceptions import ValidationError


def validate_csv(value):
    if not value.name.endswith('.csv'):
        raise ValidationError('Invalid file type')


class Deal(models.Model):
    customer = models.CharField(max_length=30)
    item = models.CharField(max_length=30)
    total = models.IntegerField()
    quantity = models.IntegerField()
    date = models.DateTimeField()


class File(models.Model):
    deals = models.FileField(upload_to=MEDIA_URL, max_length=100, blank=True, validators=[validate_csv])


class Gem(models.Model):
    name = models.CharField(max_length=50)


class Client(models.Model):
    username = models.CharField(max_length=30)
    spent_money = models.IntegerField()
    gems = models.ManyToManyField(Gem)
